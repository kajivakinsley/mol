package com.mol.Pojos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Kajiva Kinsley on 17-Jun-18.
 */

public class AddsPojo {
    @SerializedName("id")
    private int id;
    @SerializedName("user")
    private String user;
    @SerializedName("businesName")
    private String businesName;
    @SerializedName("title")
    private String title;
    @SerializedName("location")
    private String location;
    @SerializedName("story")
    private String story;
    @SerializedName("media_file")
    private String media_file;

    @SerializedName("file_id")
    private String file_id;

    @SerializedName ("date_")
    private String date_;
    @SerializedName("catagory")
    private String catagory;
    @SerializedName("isvisible")
    private boolean isvisible;



    public AddsPojo () {
    }


    public AddsPojo (int id, String user, String businesName, String title, String location,
                     String story, String media_file , String file_id, String date_, boolean isvisible , String catagory) {
        this.id = id;
        this.user = user;
        this.businesName = businesName;
        this.title = title;
        this.location = location;
        this.story = story;
        this.media_file = media_file;
        this.date_ = date_;
        this.isvisible = isvisible;
        this.catagory =catagory;
        this.file_id =  file_id ;
    }

    public String getCatagory () {
        return catagory;
    }

    public int getId () {
        return id;
    }

    public String getUser () {
        return user;
    }

    public String getBusinesName () {
        return businesName;
    }

    public String getTitle () {
        return title;
    }

    public String getLocation () {
        return location;
    }

    public String getStory () {
        return story;
    }

    public String getMedia_file () {
        return media_file;
    }

    public String getDate_ () {
        return date_;
    }

    public boolean isIsvisible () {
        return isvisible;
    }

    public String getFile_id() {
        return file_id;
    }
}
