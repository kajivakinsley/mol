package com.mol.Pojos;

import com.google.gson.annotations.SerializedName;

public class MolFiles {
    @SerializedName("file_id")
    private String file_id;
    @SerializedName("file_name")
    private String file_name;


    public MolFiles(String file_name, String file_id) {
        this.file_name = file_name;
        this.file_id = file_id;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_id() {
        return file_id;
    }

    public void setFile_id(String file_id) {
        this.file_id = file_id;
    }
}
