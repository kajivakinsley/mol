package com.mol.Pojos;

/**
 * Created by Kajiva Kinsley on 18-Jun-18.
 */

public class Home_AddsPojo extends AddsPojo{

    private int id;
    private int rating , add_id;
    private String date_ ;


    public Home_AddsPojo (int id, String user, String businesName, String title, String location,
                          String story, String media_file,String file_id, String date_, boolean isvisible, String catagory,
                          int id1, int rating, int add_id, String date_1) {
        super ( id, user, businesName, title, location, story, media_file, file_id, date_, isvisible, catagory );
        this.id = id1;
        this.rating = rating;
        this.add_id = add_id;
        this.date_ = date_1;

    }

    @Override
    public int getId () {
        return id;
    }

    public int getRating () {
        return rating;
    }

    public int getAdd_id () {
        return add_id;
    }

    @Override
    public String getDate_ () {
        return date_;
    }
}
