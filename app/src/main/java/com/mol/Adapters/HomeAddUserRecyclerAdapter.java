package com.mol.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.mol.Pojos.AddsPojo;
import com.mol.Pojos.Home_AddsPojo;
import com.mol.R;

import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.request.RequestOptions.centerCropTransform;
import static com.mol.netwox.NetGet.MAIN_URL;

/**
 * Created by Kajiva Kinsley on 18-Jun-18.
 */

public class HomeAddUserRecyclerAdapter extends RecyclerView.Adapter<HomeAddUserRecyclerAdapter.CustomViewHolder> {
    private List<AddsPojo > feedItemList;
    private Context mContext;

    public void update(ArrayList<AddsPojo> feedItemList) {
        this.feedItemList = feedItemList;
        notifyDataSetChanged();
    }
    public HomeAddUserRecyclerAdapter(Context context, List<AddsPojo > feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
    }
    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView txtTitle, txtDescription, txtDate;

        CustomViewHolder (View view) {

            super(view);
            this.imageView = (ImageView) view.findViewById( R.id.thumbnail_image);
            this.txtTitle = (TextView) view.findViewById(R.id.recycle_title);
            this.txtDescription = (TextView) view.findViewById(R.id.recycle_repeat_info);
            this.txtDate = (TextView) view.findViewById(R.id.recycle_date_time);
        }
    }
    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        AddsPojo feedItem = feedItemList.get(position);
        String title_ = feedItem.getTitle ();
        holder.txtTitle.setText(title_ + "");

        if ( Build.VERSION.SDK_INT >= 24) {
            holder.txtDescription.setText( Html.fromHtml(feedItem.getStory (), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.txtDescription.setText(Html.fromHtml(feedItem.getStory()));
        }
        holder.txtDate.setText(feedItem.getDate_ ());
       // Log.e("xxx", "onBindViewHolder: "+MAIN_URL+"media_upload/"  + feedItem.getMedia_file () );
       /* Glide.with(mContext).load( MAIN_URL+"media_upload/"  + feedItem.getMedia_file ()).thumbnail(0.1f)
                .
                placeholder(R.drawable.backup_load).fallback(R.drawable.backup_load).into(holder.imageView);*/
        Glide.with(mContext).asBitmap()
                .load(MAIN_URL+"media_upload/"  + feedItem.getMedia_file ())
                .apply(centerCropTransform().dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.backup_load)
                )
                .into(new BitmapImageViewTarget(holder.imageView) {
                    @Override
                    public void onResourceReady(Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        super.onResourceReady(resource, transition);

                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                    }
                });
    }
    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new CustomViewHolder ( LayoutInflater.from(parent.getContext()).inflate(R.layout.home_item, null));
    }
    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }
    public void setitems(ArrayList<AddsPojo> list) {
        this.feedItemList = list;
        notifyDataSetChanged();
    }

}
