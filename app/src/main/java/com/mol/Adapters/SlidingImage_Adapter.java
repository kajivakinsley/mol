package com.mol.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.mol.R;
import com.mol.activities.ZoomImageScreen;


import java.util.ArrayList;

import static com.bumptech.glide.request.RequestOptions.centerCropTransform;
import static com.mol.netwox.NetGet.MAIN_URL;

/**
 * Created by Kinsley Kajiva on 1/18/2017.
 */

public  class SlidingImage_Adapter extends PagerAdapter {

    private ArrayList <String> IMAGES_URL=new ArrayList<>();

    private LayoutInflater inflater;
    private Context context;


    public SlidingImage_Adapter(Context context,ArrayList <String> IMAGES_URL) {
        this.context = context;
        this.IMAGES_URL=IMAGES_URL;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES_URL.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.readpage_image_view_item, view, false);
         ImageView imageView = imageLayout.findViewById(R.id.imageViewReadViewPager);

        imageView.setOnClickListener(view1 ->
                context.startActivity(new Intent(
                context, ZoomImageScreen.class)
                .putExtra("zoomImageUrl",MAIN_URL + "media_upload/" +IMAGES_URL.get(position))
        ));
        /*Glide.with(this)
                .load(YOUR_URL)
                .apply(new RequestOptions().override(100, 100).placeholder(R.drawable.placeHolder).error(R.drawable.error_pic))
                .into(imageview);*/
// .override(Target.SIZE_ORIGINAL, convertView.getLayoutParams().height)
        Glide.with(context).asBitmap()
                .load( MAIN_URL + "media_upload/" +IMAGES_URL.get(position))

                .apply(
                        new RequestOptions()/*.override(100, 2100)*/.fitCenter()/*
                        centerCropTransform()*/
                                .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                 .placeholder(R.drawable.backup_load)
                )
                .into(new BitmapImageViewTarget(imageView){
                    @Override
                    public void onResourceReady(Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        super.onResourceReady(resource, transition);
                        imageView.setImageBitmap(resource);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                    }
                });


       view.addView(imageLayout);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }



}
