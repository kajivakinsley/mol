package com.mol.Adapters;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mol.Pojos.AddsPojo;
import com.mol.R;

import java.util.ArrayList;
import java.util.List;

import static com.mol.netwox.NetGet.MAIN_URL;

public class CatagoryRecyclerAdapter extends RecyclerView.Adapter<CatagoryRecyclerAdapter.CustomViewHolder> {
    private String [] feedItemList;
    private Context mContext; // CATAGORIES_ARRAY

    public void update(String [] feedItemList) {
        this.feedItemList = feedItemList;
        notifyDataSetChanged();
    }
    public CatagoryRecyclerAdapter(Context context, String [] feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
    }
    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView txtTitle;

        CustomViewHolder (View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById( R.id.thumbnail_image);
            this.txtTitle = (TextView) view.findViewById(R.id.recycle_title);

        }
    }
    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        String  feedItem = feedItemList[position];
        holder.txtTitle.setText(feedItem);


    }
    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new CustomViewHolder( LayoutInflater.from(parent.getContext()).inflate(R.layout.catagory_item, null));
    }
    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.length : 0);
    }
    public void setitems(String [] list) {
        this.feedItemList = list;
        notifyDataSetChanged();
    }
}
