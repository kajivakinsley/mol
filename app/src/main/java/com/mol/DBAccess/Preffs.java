package com.mol.DBAccess;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Kajiva Kinsley on 1/15/2018.
 */

public class Preffs {

    private Context _context;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "com.mol.preffs";

    private String USER_NAME="u_name";
    private String loggedCheck="u_loggCheck";
    private String USER_PHONE_NUMBER ="phonenumber";
    private String USER_EMAIL ="email";
    private String TERMS_AGREEED = "agreed_terms";

    private String FCM="fcm";
    private String FCMSaved="wassavedfcm";


    public boolean wasFCMSaved() {
        return  pref.getBoolean (this.FCMSaved,false);
    }

    public void setFCMSaved() {

        editor.putBoolean (this.FCMSaved,true);
        editor.commit();
    }
    public String getFCM() {
        return  pref.getString(this.FCM,"");
    }

    public void setFCM(String token) {

        editor.putString(this.FCM,token);
        editor.commit();
    }
    public String getUSER_EMAIL () {
        return  pref.getString(this.USER_EMAIL,"");
    }

    public void setUSER_EMAIL (String USER_EMAIL) {

        editor.putString(this.USER_EMAIL, USER_EMAIL );
        editor.commit();
    }

    public String getUSER_PHONE_NUMBER () {
        return  pref.getString(this.USER_PHONE_NUMBER,"");
    }

    public void setUSER_PHONE_NUMBER (String USER_PHONE_NUMBER) {

        editor.putString(this.USER_PHONE_NUMBER, USER_PHONE_NUMBER );
        editor.commit();
    }
    public int getUSER_NAME() {

        return  pref.getInt (this.USER_NAME,-1);
    }

    public void setUSER_NAME(int userId) {
        editor.putInt (this.USER_NAME,userId);
        editor.commit();
    }

    public boolean checkIfTERMS_AGREEED() {
        return  pref.getBoolean (TERMS_AGREEED,false);
    }

    public void setTERMS_AGREEED() {
        editor.putBoolean (TERMS_AGREEED,true);
        editor.commit();
    } public boolean checkIfLoggedIn() {
        return  pref.getBoolean (this.loggedCheck,false);
    }

    public void setLogStatus(boolean state) {
        editor.putBoolean (this.loggedCheck,state);
        editor.commit();
    }


    public Preffs(Context context) {
        _context= context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();

    }
}
