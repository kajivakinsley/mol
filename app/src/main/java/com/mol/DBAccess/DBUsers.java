package com.mol.DBAccess;

import io.realm.RealmObject;

/**
 * Created by Kajiva Kinsley on 1/15/2018.
 */

public class DBUsers extends RealmObject {

    public DBUsers () { }

    private String phoneNumber , password;

    private int ID;

    public String getPhoneNumber () {
        return phoneNumber;
    }

    public void setPhoneNumber (String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword () {
        return password;
    }

    public void setPassword (String password) {
        this.password = password;
    }

    public int getID () {
        return ID;
    }

    public void setID (int ID) {
        this.ID = ID;
    }
}
