package com.mol.DBAccess;

import io.realm.RealmObject;

/**
 * Created by Kajiva Kinsley on 18-Jun-18.
 */

public class DBAccessUserAddRating extends RealmObject {

    private int addID , userID;

    public DBAccessUserAddRating () {
    }

    public int getAddID () {
        return addID;
    }

    public void setAddID (int addID) {
        this.addID = addID;
    }

    public int getUserID () {
        return userID;
    }

    public void setUserID (int userID) {
        this.userID = userID;
    }
}
