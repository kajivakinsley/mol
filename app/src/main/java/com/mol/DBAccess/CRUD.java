package com.mol.DBAccess;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Kajiva Kinsley on 10-Jun-18.
 */

public class CRUD {


    public static void saveLocalRating(int addID ,int userID){
        if(isAddRatedByThisUser( addID , userID)){ // already rated ,so no need to save
            return;
        }
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        DBAccessUserAddRating rw = realm.createObject(DBAccessUserAddRating.class);
        rw.setAddID (addID);
        rw.setUserID (userID);
        realm.commitTransaction();
        realm.close();

    }
    public static boolean isAddRatedByThisUser(int addID ,int userID){
        boolean return_=false;
        Realm realm = Realm.getDefaultInstance();
        DBAccessUserAddRating results=realm.where(DBAccessUserAddRating.class).equalTo("addID",addID)
                .and ()
                .equalTo("userID",userID)
                .findFirst ();
        if(results != null){
            return_=true;
        }
        realm.close();
        return return_;
    }
}
