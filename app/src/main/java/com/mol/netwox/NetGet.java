package com.mol.netwox;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mol.Pojos.AddsPojo;
import com.mol.Pojos.MolFiles;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;


import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import java.lang.reflect.Type;

import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Kajiva Kinsley on 10-Jun-18.
 */

public class NetGet {

    public static final String MAIN_URL = "http:www.molvoke.com/admin/silentscripts/";

    public static String loginUser(String phonNumber, String password) {

        OkHttpClient client = new OkHttpClient();
      //  Log.e("vvvvv", "loginUser: " + MAIN_URL + "loginuser.php?phonNumber=" + phonNumber + "&password=" + password);

        JSONObject json;

        Request request = new Request.Builder().url(
                MAIN_URL + "loginuser.php?phonNumber=" + phonNumber + "&password=" + password
        ).build();

        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String mess = response.body().string();
                json = (new JSONObject(mess));
                //
               // Log.e("ccccc", "loginUser: " + json);
                return json.getString("message").equals("done") ?
                        json.getString("message").concat("-").concat(json.getString("userID"))
                        :
                        json.getString("message");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String registerUser(String email, String phonNumber, String password) {

        OkHttpClient client = new OkHttpClient();
        JSONObject json;
        Request request = new Request.Builder().url(MAIN_URL +
                "register_user.php?email=" + email + "&phonNumber=" + phonNumber + "&password=" + password).build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String mess = response.body().string();
                json = (new JSONObject(mess));
                return json.getString("message");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String rateAdd(String addId, String userID, String ratting) {

        OkHttpClient client = new OkHttpClient();
        JSONObject json;
        Request request = new Request.Builder().url(MAIN_URL +
                "rateAdd.php?addId=" + addId + "&userID=" + userID + "&ratting=" + ratting).build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String mess = response.body().string();
                json = (new JSONObject(mess));
                return json.getString("message");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getAddratting(String addId) {

        OkHttpClient client = new OkHttpClient();
        JSONObject json;
        Request request = new Request.Builder().url(MAIN_URL +
                "getAddRatings.php?addId=" + addId).build();
       /* Log.e("xxxxxx", "getAddratting: " +MAIN_URL +
                "getAddRatings.php?addId=" + addId );*/
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String mess = response.body().string();
                // Log.e("xx222", "getAddratting: " +mess );
                json = (new JSONObject(mess));
                return json.getString("message");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return "";
    }
    //getAddRatings.php

    public static ArrayList<AddsPojo> getListingsByCatagory(String catagory) {
        ArrayList op = new ArrayList();
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("cata", catagory)
                .build();
        Request request = new Request.Builder()
                .url(MAIN_URL + "getCatagory.php").post(body).build();
        //Log.e("xxxx", "getListingsByCatagory: " + MAIN_URL+"getCatagory.php?cata=" + catagory);
        try {
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            ///Log.e("xxx", "getListings: " +responseString );
            JSONObject jsonObject = new JSONObject(responseString);
            String jdata = jsonObject.getString("results");
            // Log.e("xxx", "11getListings: " +jdata );
            Gson gson = new Gson();
            Type collection = new TypeToken<ArrayList<AddsPojo>>() {
            }.getType();
            op = gson.fromJson(jdata, collection);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return op;
    }

    public static ArrayList<AddsPojo> getListings(String search) {
        ArrayList op = new ArrayList();
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("poST_search", search)
                .build();
        Request request = new Request.Builder()
                .url(MAIN_URL + "getallAdds.php").post(body).build();
        try {
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            ///Log.e("xxx", "getListings: " +responseString );
            JSONObject jsonObject = new JSONObject(responseString);
            String jdata = jsonObject.getString("results");
            // Log.e("xxx", "11getListings: " +jdata );
            Gson gson = new Gson();
            Type collection = new TypeToken<ArrayList<AddsPojo>>() {
            }.getType();
            op = gson.fromJson(jdata, collection);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return op;
    }


    private static String getMimeType(String path) {

        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(path));
    }

    public static String saveAddPost(String sourceImageFile, String businesName_, String title_, String location_, String story_, String catagor_, String userID) {
        try {
            File sourceFile = new File(sourceImageFile);
            String content_type = getMimeType(sourceFile.getPath());

            String filename = sourceImageFile.substring(sourceImageFile.lastIndexOf("/") + 1);
            RequestBody file_body = RequestBody.create(MediaType.parse(content_type), sourceFile);

            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("type", content_type)
                    .addFormDataPart(
                            "uploaded_file",
                            filename,
                            file_body
                    )
                    .build();


            Request request = new Request.Builder()
                    .url(
                            MAIN_URL + "save_adds.php?businesName=" + businesName_ + "&title=" + title_ + "&location=" + location_
                                    + "&story=" + story_ + "&catagor=" + catagor_ + "&userID=" + userID
                    ) //192.168.100.99
                    .post(requestBody)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            String res = new JSONObject(response.body().string()).getString("message");
          //  Log.e("xxx", "xxxxxbbbbbb: " + res);

            return (res);

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            //Log.e("xxx", "Error: " + e.getLocalizedMessage());
        } catch (Exception e) {
            String ErrorMessage = "";
            if (ErrorMessage.contains("failed to connect to")) {
                Log.e("xxx ", "Other 1Error:Connection error");
            }
           // Log.e("xxx", "Other Error: " + e.getLocalizedMessage());
        }
        return "";
    }

    public static String savePostData(List<String> fileslink, String businesName_, String title_, String location_, String story_, String catagor_, String userID) {
        try {
            MultipartBody.Builder mRequestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM);
            for (int i = 0; i < fileslink.size(); i++) {
                //String filename = fileslink.get(i);
                String filename = fileslink.get(i).substring(fileslink.get(i).lastIndexOf("/") + 1);
                File f = new File(fileslink.get(i));
                String  content_type = getMimeType(f.getPath());

                mRequestBody.addFormDataPart("uploaded_file" + i,filename,RequestBody.create(MediaType.parse(content_type),f));
            }
            RequestBody rb = mRequestBody.build();
            Request request = new Request.Builder()
                    .url(MAIN_URL + "save_adds.php?businesName=" + businesName_ + "&title=" + title_ + "&location=" + location_
                            + "&story=" + story_ + "&catagor=" + catagor_ + "&userID=" + userID)
                    .post(rb)
                    .build();
            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            String respon_ = response.body().string();

            String res = new JSONObject(respon_).getString("message");
            //Log.e("xxx", "hhhhhb: " + res);
            return (res);


        } catch (UnknownHostException | UnsupportedEncodingException e) {
           // Log.e("xxx", "Error: " + e.getLocalizedMessage());
        } catch (Exception e) {
            String ErrorMessage = "";
            if (ErrorMessage.contains("failed to connect to")) {
             //   Log.e("xxx ", "Other 1Error:Connection error");
            }
          //  Log.e("xxx", "Other Error: " + e.getLocalizedMessage());
        }
        return "";
    }

    public static ArrayList<MolFiles>  getAlbumPhotos(String file_id){
        List<MolFiles> op = null;
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("poST_table","8")
                .build();
        Request request = new Request.Builder()
                .url(MAIN_URL + "getPhotos.php?file_id="+file_id).post(body).build();
       // Log.e("ppp", "getAlbumPhotos: "+MAIN_URL + "getPhotos.php?file_id="+file_id );
        try {
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            String res = new JSONObject(responseString).getString("message");
           // Log.e("xxx", "getAlbumPhotos: " +  responseString);
            Gson gson = new Gson();
            Type collection = new TypeToken<ArrayList<MolFiles>>(){}.getType();
            op = gson.fromJson(res, collection);
        } catch (IOException  e) {e.printStackTrace();} catch (JSONException e) {
            e.printStackTrace();
        }
        return (ArrayList<MolFiles>) op;



    }

}
