package com.mol.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.mol.Adapters.SlidingImage_Adapter;
import com.mol.DBAccess.Preffs;
import com.mol.Pojos.MolFiles;
import com.mol.R;
import com.mol.cwidgets.TouchImageView;
//import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.bumptech.glide.request.RequestOptions.centerCropTransform;
import static com.mol.DBAccess.CRUD.isAddRatedByThisUser;
import static com.mol.DBAccess.CRUD.saveLocalRating;
import static com.mol.netwox.NetGet.MAIN_URL;
import static com.mol.netwox.NetGet.getAddratting;
import static com.mol.netwox.NetGet.getAlbumPhotos;
import static com.mol.netwox.NetGet.rateAdd;
import static com.mol.utils.utils.CATAGORIES_ARRAY;
import static com.mol.utils.utils.indexOfArray;

public class ListingReadPage extends BaseActivity {

    private String[] passdData = {"", "", "", "", "", "","","",""};
    private TouchImageView mImage;
    private TextView title, Descriptions;
    private RatingBar ratingBar;
    private String ADDID = ""  ,IMAGE_URL = "";
    private boolean isUserRated = false;
    private Preffs preffs ;
    private FloatingActionButton fab;
    private CollapsingToolbarLayout toolbar_layout;
    private AppBarLayout app_bar;
    private  Toolbar toolbar;
    private final long DELAY_T = 3800;
    ///////////
    private ArrayList<String> IMAGES_URL = new ArrayList<>();
  //  private CirclePageIndicator mPagerIndicator;
    private ViewPager mPager;
    //private CirclePageIndicator mPagerIndicator;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ImageButton nxt, prev;
    private Timer swipeTimer;
    private Runnable Update;
    private Handler handler = new Handler();
    private String Album_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_read_page);
         toolbar = findViewById(R.id.toolbar);
        app_bar= findViewById(R.id.app_bar);
        toolbar_layout= findViewById(R.id.toolbar_layout);
        initObjects ();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle("Read Page");
        initViews();


        Bundle b = this.getIntent().getExtras();

        String[] dataOp = b.getStringArray("id_catFrag");
        passdData = dataOp;
        //Log.e("xxxxxx", "onCreate: " + passdData .length );
        for (String it :
                passdData ) {
          //  Log.e("xxxxxx", "onCreate:111 " + it );
        }

        ADDID = dataOp[3];
        title.setText(dataOp[7]);
        Descriptions.setText(
                "Business Name: " + dataOp[0] + " \n\n" +
                        "Business Catagory: " + CATAGORIES_ARRAY[ indexOfArray(dataOp[1]) == -1 ? 0 : indexOfArray(dataOp[1])  ]  + " \n\n" +
                        "Business Location: " + dataOp[4] + "\n\n" +
                        "Business Story: " + dataOp[6] + " \n\n\n\n"
        );

IMAGE_URL = MAIN_URL + "media_upload/" + dataOp[5];
        Glide.with(this).asBitmap()
                .load(MAIN_URL + "media_upload/" + dataOp[5])
                .apply(centerCropTransform().dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.backup_load)
                )
                .into(new BitmapImageViewTarget(mImage) {
                    @Override
                    public void onResourceReady(Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        super.onResourceReady(resource, transition);
                        mImage.setImageBitmap(resource);

                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                    }
                });
        setViewsValues();
        initListerners();
            getimagesForSlide();


    }

    private void getimagesForSlide() {
        new AsyncTask<Void,Void, ArrayList<MolFiles>>(){

            @Override
            protected ArrayList<MolFiles> doInBackground(Void... voids) {
              //  Log.e("xxxx", "doInBackground:234 " );

                return getAlbumPhotos(passdData[8]);
            }

            @Override
            protected void onPostExecute(ArrayList<MolFiles> molFiles) {
                super.onPostExecute(molFiles);
                //Log.e("xxxx", "doInBackground:777 " );
               // Log.e("ccccc", "onPostExecute: " + molFiles );
                if (molFiles != null) {
                   // Log.e("xxxx", "doInBackground:8888 " );
                    for (MolFiles f : molFiles) {
                        IMAGES_URL.add(f.getFile_name());
                    }
                    initImageSlider();
                }
            }
        }.execute();
    }

    public void initImageSlider() {

        mPager.setAdapter(new SlidingImage_Adapter(this, IMAGES_URL));
        final float density = getResources().getDisplayMetrics().density;
       // mPagerIndicator.setRadius(3 * density);
      //      mPagerIndicator.setViewPager(mPager);
        NUM_PAGES = IMAGES_URL.size();
        // Auto start of viewpager
        Update = () -> {
            if (currentPage == NUM_PAGES) {
                currentPage = 0;
            }
            mPager.setCurrentItem(currentPage++, true);
        };
        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_T, DELAY_T);


// Pager listener over indicator
        /*mPagerIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int pos) {
            }
        });*/
    }
    public void ShowDialog() {
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(this);
        final RatingBar rating = new RatingBar(this);
        rating.setMax(5);

        popDialog.setIcon(android.R.drawable.btn_star_big_on);
        popDialog.setTitle("Rate! ");
        popDialog.setView(rating);

        // Button OK
        popDialog.setPositiveButton(android.R.string.ok,
                (dialog, which) -> {
                    dialog.dismiss();
                    saveRating(String.valueOf(rating.getProgress()));
                })

                // Button Cancel
                .setNegativeButton("Cancel",
                        (dialog, id) -> dialog.cancel());

        popDialog.create();
        popDialog.show();

    }

    private void showRteDialog(){
        Dialog rankDialog = new Dialog(this);
        rankDialog.setContentView(R.layout.rate_dialog);
        rankDialog.setCancelable(true);
        RatingBar ratingBar = rankDialog.findViewById(R.id.dialog_ratingbar);
        ratingBar.setRating(0);

        TextView text = rankDialog.findViewById(R.id.rank_dialog_text1);


        Button updateButton = rankDialog.findViewById(R.id.rank_dialog_button);
        updateButton.setOnClickListener(v -> {
            rankDialog.dismiss();
            saveRating(String.valueOf(ratingBar.getProgress()));
        });
        //now that the dialog is set up, it's time to show it
        rankDialog.show();
    }

    private void saveRating(String ratting) {
        new AsyncTask<String, Void, String>() {
            ProgressDialog progressDialog;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(ListingReadPage.this);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Rating .....");
                progressDialog.show();

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                if (s.equals("done")) {
                    int user = preffs.getUSER_NAME ();
                    saveLocalRating(Integer.parseInt(ADDID), user);
                }
                //Log.e("xxx", "onPostExecute: " + s);
            }

            @Override
            protected String doInBackground(String... voids) {
                return rateAdd("1", "1", voids[0]);
            }

        }.execute(ratting);
    }


    @Override
    protected void initViews() {
        mImage = findViewById(R.id.mImage);
        title = findViewById(R.id.title);
        ratingBar = findViewById(R.id.ratingBar);
        Descriptions = findViewById(R.id.Descriptions);
        fab = findViewById(R.id.fab);
        FrameLayout readpage_sec_frameviewpager = findViewById(R.id.readpage_sec_frameviewpager);

        mPager = findViewById(R.id.viewpager);
        //mPagerIndicator = (CirclePageIndicator) findViewById(R.id.pager_indicator);
        nxt = findViewById(R.id.right_nav);
        prev = findViewById(R.id.left_nav);
        readpage_sec_frameviewpager.setVisibility(View.VISIBLE);

    }

    @Override
    protected void initListerners() {
        fab.setOnClickListener(ev ->
           // ShowDialog()
                showRteDialog()
        );
        prev.setOnClickListener(Ev->{
            if (currentPage == NUM_PAGES) {
                currentPage = 0;
            }
            currentPage = currentPage > 0 ?currentPage -- : (NUM_PAGES -1) ;
            mPager.setCurrentItem(currentPage, true);
        });
        nxt.setOnClickListener(ev->{
            if (currentPage == NUM_PAGES) {
                currentPage = 0;
            }
            mPager.setCurrentItem(currentPage++, true);
        
    });
        /*mImage.setOnClickListener(ev->{
            startActivity(
                    new Intent(ListingReadPage.this , ZoomImageScreen.class)
                    .putExtra("zoomImageUrl" ,IMAGE_URL )
            );
        });*/
    }

    @Override
    protected void setViewsValues() {
        toolbar.setTitle("");
        toolbar_layout.setTitleEnabled(false);
        app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            boolean isVisible = true;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    toolbar.setTitle("Details ");
                    isVisible = true;
                } else if(isVisible) {
                    toolbar.setTitle("");
                    isVisible = false;
                }
            }
        });
        int user = preffs.getUSER_NAME ();
       // Log.e("vvvv", "initObjects: "+ADDID );
     //   Log.e("bbbbb", "initObjects: "+user );
        isUserRated = isAddRatedByThisUser (Integer.parseInt(ADDID), user );
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {
                return getAddratting(ADDID);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
              //  Log.e("xxxxxx", "onPostExecute: Rattong" + s);

                float rateAvg = s.isEmpty() ? 00 : Float.parseFloat((s));

                ratingBar.setRating( rateAvg);
                // Toast.makeText ( HomeAddUserReadPage.this, "rating is " + s, Toast.LENGTH_LONG ).show ();
            }
        }.execute();
    }

    @Override
    protected void initObjects() {
        preffs = new Preffs ( this );

    }
}
