package com.mol.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.mol.DBAccess.Preffs;
import com.mol.R;

public class Terms extends BaseActivity {

    private Button btnproceed;
    private  Preffs preff ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        preff =  new Preffs(Terms.this);
        if(preff.checkIfTERMS_AGREEED()){
            clearAllActivities(this ,MainActivity.class );
        }
        initViews();
        initListerners();
    }

    @Override
    protected void initViews() {
        btnproceed = findViewById(R.id.btnproceed);
    }

    @Override
    protected void initListerners() {
        btnproceed.setOnClickListener(ev->{
            preff.setTERMS_AGREEED();
            Toast.makeText(this, "Agreed to Terms", Toast.LENGTH_SHORT).show();
            clearAllActivities(Terms.this , MainActivity.class);
        });
    }

    @Override
    protected void setViewsValues() {

    }

    @Override
    protected void initObjects() {

    }
}
