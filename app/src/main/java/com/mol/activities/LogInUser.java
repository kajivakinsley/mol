package com.mol.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.mol.DBAccess.Preffs;
import com.mol.R;
import com.mol.cwidgets.customfonts.MyEditText;
import com.mol.messages.SeeTastyToast;

import messages.NifftyDialogs;

import static com.mol.netwox.NetGet.loginUser;
import static com.mol.netwox.NetGet.registerUser;
import static com.mol.utils.utils.isValidPhoneNumber;
import static com.mol.utils.utils.isvalidEmail;

public class LogInUser extends BaseActivity {
    private MyEditText phonNumber, password, con_password;
    private Button btnRegister, btnLogin;
    private Context context = LogInUser.this;
    private Preffs preffs;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        initObjects ();
        setContentView ( R.layout.activity_log_in_user );
        initViews ();
        initListerners ();
        /*if(!preffs.checkIfLoggedIn () ) {
            preffs.setLogStatus(true);
            preffs.setUSER_PHONE_NUMBER("0737531075");
            preffs.setUSER_NAME(1);
            startActivity(new Intent(context, LoogedUser.class));
            finish();
        }*/
    }
    private void showProgressDialog (final boolean isToShow) {

        if ( isToShow ) {
            if ( ! progressDialog.isShowing () ) {
                progressDialog.setMessage ( "Processing..." );
                progressDialog.setCancelable ( false );
                progressDialog.show ();
            }
        } else {
            if ( progressDialog.isShowing () ) {
                progressDialog.dismiss ();
            }
        }

    }
    @Override
    protected void initViews () {
        getSupportActionBar().setTitle ( "Sign In" );

        phonNumber = findViewById ( R.id.phoneNumber );;
        password = findViewById ( R.id.password );

        btnRegister = findViewById ( R.id.btnRegister );
        btnLogin = findViewById ( R.id.btnLogin );
    }

    @Override
    protected void initListerners () {
        btnLogin.setOnClickListener ( sc->{
            final String phonNumber_ = phonNumber.getText ().toString ().trim ();
            final String password_ = password.getText ().toString ().trim ();
            if ( phonNumber_.isEmpty () ) {
                phonNumber.setError ( "Phone Number can't be empty" );
                return;
            }
            if(!isValidPhoneNumber ( phonNumber_ )){
                phonNumber.setError ( "Put valid Number" );
                return;
            }

            if ( password_.isEmpty () ) {
                password.setError ( "Password can't be empty" );
                return;
            }
            showProgressDialog ( true );
            new AsyncTask<Void,Void,String> (){

                @Override
                protected String doInBackground (Void... voids) {

                    return loginUser(phonNumber_ , password_);
                }

                @Override
                protected void onPostExecute (String s) {
                    super.onPostExecute ( s );
                    showProgressDialog ( false);
                  //  Log.e("xxx", "loginUser: "+s);
                  //  Log.e("xxxx9xx", "onPostExecute: " +s.split("-")[1]  );
                    //Log.e("xxxx999xx", "onPostExecute: " +s.split("-")[0]  );
                    if(s.contains ( "done" )){
                        preffs.setLogStatus ( true );
                        preffs.setUSER_PHONE_NUMBER ( phonNumber_ );
                        preffs.setUSER_NAME(Integer.parseInt(s.split("-")[1]));
                        startActivity ( new Intent ( context , LoogedUser.class ) );
                        finish ();
                    }
                    if(s.equals ( "unfound" )) {
                        new NifftyDialogs (context).messageOkError("Log in Failed", "Please Register to lo gin");
                        Toast.makeText ( context, "Log in Failed", Toast.LENGTH_SHORT ).show ();

                    }

                    if(s.isEmpty ()){
                        new NifftyDialogs(context).messageOkError("Connection Failed","try again");
                        //   Toast.makeText ( SignIn.this, "Connection Failed , Try again", Toast.LENGTH_SHORT ).show ();
                    }

                }
            }.execute();
        } );




        btnRegister.setOnClickListener ( wc->{
            startActivity ( new Intent ( context , RegisterUser.class ) );
        } );
    }

    @Override
    protected void setViewsValues () {

    }

    @Override
    protected void initObjects () {
        progressDialog = new ProgressDialog ( this );
        preffs = new Preffs(this);
    }
}
