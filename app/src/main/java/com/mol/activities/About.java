package com.mol.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mol.R;

public class About extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle("About Us");

    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void initListerners() {

    }

    @Override
    protected void setViewsValues() {

    }

    @Override
    protected void initObjects() {

    }
}
