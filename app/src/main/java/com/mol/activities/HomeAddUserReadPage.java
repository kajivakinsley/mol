package com.mol.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RatingBar;
import android.widget.Toast;

import com.mol.DBAccess.Preffs;
import com.mol.R;

import static com.mol.DBAccess.CRUD.isAddRatedByThisUser;
import static com.mol.netwox.NetGet.getAddratting;
import static com.mol.netwox.NetGet.rateAdd;
import static com.mol.netwox.NetGet.saveAddPost;

public class HomeAddUserReadPage extends BaseActivity {
    private boolean isUserRated = false;
    private Preffs preffs = new Preffs ( this );

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_home_add_user_read_page );
        initObjects ();
        Toolbar toolbar = findViewById ( R.id.toolbar );
        setSupportActionBar ( toolbar );


        initViews ();
        setViewsValues ();
        initListerners ();


    }

    public void ShowDialog () {
        final AlertDialog.Builder popDialog = new AlertDialog.Builder ( this );
        final RatingBar rating = new RatingBar ( this );
        rating.setMax ( 5 );
       // rating.setStepSize(0.5);



        popDialog.setIcon ( android.R.drawable.btn_star_big_on );
        popDialog.setTitle ( "Rate! " );
        popDialog.setView ( rating );

        // Button OK
        popDialog.setPositiveButton ( android.R.string.ok,
                (dialog, which) -> {
                    dialog.dismiss ();
                    saveRating ( String.valueOf ( rating.getProgress () ) );
                } )

                // Button Cancel
                .setNegativeButton ( "Cancel",
                        (dialog, id) -> dialog.cancel () );

        popDialog.create ();
        popDialog.show ();

    }

    private void saveRating (String ratting) {
        new AsyncTask < String, Void, String > () {
            ProgressDialog progressDialog;


            @Override
            protected void onPreExecute () {
                super.onPreExecute ();
                progressDialog = new ProgressDialog ( HomeAddUserReadPage.this );
                progressDialog.setCancelable ( false );
                progressDialog.setMessage ( "Rating ....." );
                progressDialog.show ();

            }

            @Override
            protected void onPostExecute (String s) {
                super.onPostExecute ( s );
                if ( progressDialog != null ) {
                    progressDialog.dismiss ();
                }
                if ( s.equals ( "done" ) ) {

                }
                Log.e ( "xxx", "onPostExecute: " + s );
            }

            @Override
            protected String doInBackground (String... voids) {
                return rateAdd ( "1", "1", voids[0] );
            }

        }.execute (ratting);
    }

    @Override
    protected void initViews () {
        FloatingActionButton fab = findViewById ( R.id.fab );
        fab.setOnClickListener ( view ->
                ShowDialog () );
    }

    @Override
    protected void initListerners () {

    }

    @Override
    protected void setViewsValues () {
        new AsyncTask < Void, Void, String > () {

            @Override
            protected String doInBackground (Void... voids) {
                return getAddratting ( 1 + "" );
            }

            @Override
            protected void onPostExecute (String s) {
                super.onPostExecute ( s );
                Toast.makeText ( HomeAddUserReadPage.this, "rating is " + s, Toast.LENGTH_LONG ).show ();
            }
        }.execute ();
    }

    @Override
    protected void initObjects () {
        int user = preffs.getUSER_NAME ();
        isUserRated = isAddRatedByThisUser ( 1, 1 );

    }
}
