package com.mol.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.mol.DBAccess.Preffs;
import com.mol.R;
import com.mol.cwidgets.customfonts.MyEditText;
import com.mol.messages.SeeTastyToast;

import messages.NifftyDialogs;

import static com.mol.netwox.NetGet.registerUser;
import static com.mol.utils.utils.isValidPhoneNumber;
import static com.mol.utils.utils.isvalidEmail;

public class RegisterUser  extends BaseActivity {
    private MyEditText email ,phonNumber, password;
    private Button btnRegister, btnBacktoLohin;
    private Context context = RegisterUser.this;
    private Preffs preffs;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        initObjects ();
        setContentView ( R.layout.activity_register );
        initViews ();
        initListerners ();
    }

    private void showProgressDialog (final boolean isToShow) {

        if ( isToShow ) {
            if ( ! progressDialog.isShowing () ) {
                progressDialog.setMessage ( "Processing..." );
                progressDialog.setCancelable ( false );
                progressDialog.show ();
            }
        } else {
            if ( progressDialog.isShowing () ) {
                progressDialog.dismiss ();
            }
        }

    }

    @Override
    protected void initViews () {
        getSupportActionBar().setTitle ( "Sign Up" );
        email = findViewById ( R.id.email );
        phonNumber = findViewById ( R.id.phoneNumber );;
        password = findViewById ( R.id.password );

        btnRegister = findViewById ( R.id.btnRegister );
        btnBacktoLohin = findViewById ( R.id.btnBacktoLohin );

    }

    @Override
    protected void initListerners () {
        btnBacktoLohin.setOnClickListener ( er->{
            onBackPressed ();
        });
        btnRegister.setOnClickListener ( ev->{
            final String email_ = email.getText ().toString ().trim ();
            final String phonNumber_ = phonNumber.getText ().toString ().trim ();
            final String password_ = password.getText ().toString ().trim ();

            if ( email_.isEmpty () ) {
                email.setError ( "Email can't be empty" );
                return;
            }
            if ( phonNumber_.isEmpty () ) {
                phonNumber.setError ( "Phone Number can't be empty" );
                return;
            }
            if(!isValidPhoneNumber ( phonNumber_ )){
                phonNumber.setError ( "Put valid Number" );
                return;
            }
            if ( !isvalidEmail(email_ )) {
                email.setError ( "Please put a Valid Email" );
                return;
            }
            if ( password_.isEmpty () ) {
                password.setError ( "Passsword can't be empty" );
                return;
            }

            showProgressDialog ( true );
            new AsyncTask<Void,Void,String> (){

                @Override
                protected String doInBackground (Void... voids) {
                    return registerUser(email_ ,phonNumber_ , password_);
                }

                @Override
                protected void onPostExecute (String s) {
                    super.onPostExecute ( s );
                    showProgressDialog ( false);
                 //   Log.e("xxxxxx", "onPostExecute: " + s );
                    if(s.equals ( "done" )){

                        preffs.setLogStatus ( false );
                        preffs.setUSER_EMAIL ( email_ );
                        preffs.setUSER_PHONE_NUMBER ( phonNumber_ );

                        new SeeTastyToast ( context ).ToastSuccess ( "You can Log In !" );
                        startActivity ( new Intent ( context , LogInUser.class ) );
                        finish ();
                    }
                    if(s.equals ( "failed" )) {
                        new NifftyDialogs(context).messageOkError("Connection Failed", "Failed to Save,try again");
                        Toast.makeText ( context, "Failed to Save", Toast.LENGTH_SHORT ).show ();

                    }
                    if(s.equals ( "found" )){
                        Toast.makeText ( context, "Already Registerd, Please log in", Toast.LENGTH_SHORT ).show ();
                    }
                    if(s.isEmpty ()){
                        new NifftyDialogs(context).messageOkError("Connection Failed","try again");
                        //   Toast.makeText ( SignIn.this, "Connection Failed , Try again", Toast.LENGTH_SHORT ).show ();
                    }

                }
            }.execute();

        } );
    }

    @Override
    protected void setViewsValues () {

    }

    @Override
    protected void initObjects () {
        progressDialog = new ProgressDialog ( this );
        preffs = new Preffs(this);
    }
}
