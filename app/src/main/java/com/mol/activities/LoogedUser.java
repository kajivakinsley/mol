package com.mol.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import com.mol.DBAccess.Preffs;
import com.mol.R;
import com.mol.fragments.loogeduser.AddPost_fragment;
import com.mol.fragments.loogeduser.MylistingFragment;
import com.mol.fragments.loogeduser.PricingFragment;
import com.mol.fragments.user.Catagories_fragment;
import com.mol.fragments.user.HomeFragment;
import com.mol.fragments.user.NewItems_fragment;

import java.util.ArrayList;
import java.util.List;

public class LoogedUser extends BaseActivity {
    private Context context = LoogedUser.this;
    private Preffs preffs;
    private ViewPager pager;
    private  Toolbar toolbar;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
                switch (item.getItemId ()) {
                    case R.id.nav_listing:
                        pager.setCurrentItem ( 0 );
                        return true;
                    case R.id.nav_pricing:
                        pager.setCurrentItem ( 1 );
                        return true;
                    case R.id.nav_post_add:
                        pager.setCurrentItem ( 2 );
                        return true;
                }
                return false;
            };

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_looged_user );
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Home");
        initObjects();
        if(!preffs.checkIfLoggedIn () ){
            startActivity(new Intent(this , LogInUser.class));
            finish();
            return;
        }
        BottomNavigationView navigation = findViewById ( R.id.navigation );
        navigation.setOnNavigationItemSelectedListener ( mOnNavigationItemSelectedListener );
        initViews ();
        initListerners ();
        setViewsValues ();

    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter (getSupportFragmentManager());
        adapter.addFrag(new MylistingFragment (), "");
        adapter.addFrag(new PricingFragment (), "");
        adapter.addFrag ( new AddPost_fragment (),"" );
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setCurrentItem( 0 );

    }

    @Override
    protected void initViews () {
        pager = findViewById(R.id.viewpager);
       setupViewPager(pager);
    }

    @Override
    protected void initListerners () {

    }

    @Override
    protected void setViewsValues () {

    }

    @Override
    protected void initObjects () {
        preffs = new Preffs(context);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment > mFragmentList = new ArrayList<> ();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);

        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
