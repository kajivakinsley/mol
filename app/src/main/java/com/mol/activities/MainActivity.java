package com.mol.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.mol.DBAccess.Preffs;
import com.mol.R;
import com.mol.fragments.loogeduser.AddPost_fragment;
import com.mol.fragments.user.Catagories_fragment;
import com.mol.fragments.user.HomeFragment;
import com.mol.fragments.user.NewItems_fragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {


    private Preffs preffs;
    private ViewPager pager;


    private Context context = MainActivity.this;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener () {

        @Override
        public boolean onNavigationItemSelected (@NonNull MenuItem item) {
            switch (item.getItemId ()) {
                case R.id.nav_home:
                    pager.setCurrentItem ( 2 );

                    return true;
                case R.id.nav_catagories:
                    pager.setCurrentItem ( 1 );
                    return true;
                case R.id.nav_new:
                    pager.setCurrentItem ( 2 );
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );

        setContentView ( R.layout.activity_main );
        initObjects();
        BottomNavigationView navigation = findViewById ( R.id.navigation );
        navigation.setOnNavigationItemSelectedListener ( mOnNavigationItemSelectedListener );
       // getSupportActionBar().setIcon(R.drawable.logo);
        getSupportActionBar().setTitle("Home");
        initViews ();
        initListerners ();
        setViewsValues ();
        if(preffs.getUSER_EMAIL ().isEmpty ()){

        }
    }

    @Override
    protected void initViews () {
        pager = findViewById(R.id.viewpager);
        setupViewPager(pager);
    }

    @Override
    protected void initListerners () {

    }

    @Override
    protected void setViewsValues () {

    }

    @Override
    protected void initObjects () {
        preffs = new Preffs(context);

        if(!preffs.checkIfTERMS_AGREEED()){

            clearAllActivities(this ,Terms.class );
            finish();
            return;
        }
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new NewItems_fragment (), "");
        adapter.addFrag(new Catagories_fragment (), "");
        adapter.addFrag ( new HomeFragment (),"" );

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setCurrentItem( 2 );

    }
    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment > mFragmentList = new ArrayList<> ();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);

        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.loginout:
                startActivity ( new Intent ( context , LoogedUser.class ) );
                preffs.setLogStatus(false);

                break;
                case R.id.about:
                    startActivity ( new Intent ( context , About.class ) );
                    break;
            default:
                break;
        }
        return true;
    }
}
