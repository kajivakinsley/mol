package com.mol.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.mol.R;
import com.mol.cwidgets.TouchImageView;
import com.mol.messages.SeeToast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import com.bumptech.glide.request.transition.Transition;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.bumptech.glide.request.RequestOptions.centerCropTransform;
import static com.mol.utils.utils.getStatusBarHeight;
import static com.mol.utils.utils.insertImage;

public class ZoomImageScreen extends BaseActivity {
    private int statusBarHeight;
    private Toolbar toolbar;
    boolean isStatusBarTransparent = false;
    private Context context = ZoomImageScreen.this;
    private String imgUrl;
    private TouchImageView imageView;
    private Bitmap bmp = null;
    private boolean hasImageLoaded = false;
    private Intent shareIntent;
    public static final int PERMISSION_REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initObjects();
        setContentView(R.layout.activity_zoom_image_screen);

        initViews();
        setViewsValues();
        initListerners();
        setcomponents();


    }

    @Override
    protected void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        imageView = (TouchImageView) findViewById(R.id.zoomimage);
    }

    @Override
    protected void initListerners() {

    }

    @Override
    protected void setViewsValues() {

    }

    @Override
    protected void initObjects() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE|View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_trans80));

            statusBarHeight = getStatusBarHeight(this);
            isStatusBarTransparent = true;
        }
        imgUrl = getIntent().getStringExtra("zoomImageUrl");
    }
    protected void setcomponents() {

        Glide.with(context).asBitmap()
                .load(imgUrl)
                .apply(new RequestOptions()/*.override(100, 2100)*/.fitCenter().dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.backup_load)
                )
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    public void onResourceReady(Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        super.onResourceReady(resource, transition);
                        imageView.setImageBitmap(resource);
                        hasImageLoaded = true;
                        bmp = resource;
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                    }
                });


    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean writeExternalstorage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (writeExternalstorage) {
                        insertImage(getContentResolver(), bmp, getApplicationContext().getString(getApplicationContext().getApplicationInfo().labelRes).replace(" ", "") + Calendar.getInstance().getTimeInMillis(), "");

                        new SeeToast().message_long(context, "Image Saved in Gallery Successfully!");
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
                                showMessageOKCancel("You need to allow  permission,otherwise you wont Save Images",
                                        (dialog, which) -> {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
        }
    }
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case /*R.id.action_save*/2:


                if (hasImageLoaded) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!checkPermission()) {
                            requestPermission();
                        } else {
                            insertImage(getContentResolver(), bmp, getApplicationContext().getString(getApplicationContext().getApplicationInfo().labelRes).replace(" ", "") + Calendar.getInstance().getTimeInMillis(), "");

                            new SeeToast().message_long(context, "Image Saved in Gallery Successfully!");

                        }
                    } else {
                        insertImage(getContentResolver(), bmp, getApplicationContext().getString(getApplicationContext().getApplicationInfo().labelRes).replace(" ", "") + Calendar.getInstance().getTimeInMillis(), "");

                        new SeeToast().message_long(context, "Image Saved in Gallery Successfully!");
                    }

                } else {
                    new SeeToast().message_short(context, "Image Still Loading!");
                }

                return true;
            case/* R.id.action_share*/1:
                if (hasImageLoaded) {
                    prepareShareIntent();

                } else {
                    new SeeToast().message_short(context, "Image Still Loading!");
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void prepareShareIntent() {

        // Construct share intent as described above based on bitmap

        shareIntent = new Intent();

        shareIntent.setAction(Intent.ACTION_SEND);

        Uri bmpUri = getLocalBitmapUri(bmp); // see previous remote images section

        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "Share ..."));

    }

    private Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            bmpUri = Uri.fromFile(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }
}
