package com.mol.fragments.user;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mol.Adapters.CatagoryRecyclerAdapter;
import com.mol.Adapters.MylistingRecyclerAdapter;
import com.mol.Pojos.AddsPojo;
import com.mol.R;
import com.mol.activities.CatagorySearchListing;
import com.mol.activities.ListingReadPage;
import com.mol.cwidgets.LinearItermDecorator;
import com.mol.cwidgets.MyRecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.SlideInBottomAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.SlideInRightAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;
import jp.wasabeef.recyclerview.animators.FadeInDownAnimator;
import jp.wasabeef.recyclerview.animators.FadeInLeftAnimator;
import jp.wasabeef.recyclerview.animators.FadeInRightAnimator;
import jp.wasabeef.recyclerview.animators.FadeInUpAnimator;
import jp.wasabeef.recyclerview.animators.FlipInBottomXAnimator;
import jp.wasabeef.recyclerview.animators.FlipInLeftYAnimator;
import jp.wasabeef.recyclerview.animators.FlipInRightYAnimator;
import jp.wasabeef.recyclerview.animators.FlipInTopXAnimator;
import jp.wasabeef.recyclerview.animators.LandingAnimator;
import jp.wasabeef.recyclerview.animators.OvershootInLeftAnimator;
import jp.wasabeef.recyclerview.animators.OvershootInRightAnimator;
import jp.wasabeef.recyclerview.animators.ScaleInAnimator;
import jp.wasabeef.recyclerview.animators.ScaleInBottomAnimator;
import jp.wasabeef.recyclerview.animators.ScaleInLeftAnimator;
import jp.wasabeef.recyclerview.animators.ScaleInRightAnimator;
import jp.wasabeef.recyclerview.animators.ScaleInTopAnimator;
import jp.wasabeef.recyclerview.animators.SlideInDownAnimator;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.SlideInRightAnimator;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

import static com.mol.netwox.NetGet.getListings;
import static com.mol.utils.utils.CATAGORIES_ARRAY;

/**
 * Created by Kajiva Kinsley on 10-Jun-18.
 */

public class Catagories_fragment extends Fragment {
    private View layout;

    private TextView recyclerview_status;
    private RecyclerView recyclerview;
    private ProgressDialog progressDialog;
    private final static int VERTICAL_ITEM_SPACE = 4;//14
    private CatagoryRecyclerAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate( R.layout.catagories_fragment, container, false);
        initObjects();
        initViews();
        setViewsValues();
        initListerners();
        return layout;
    }
    private void initObjects() {
        progressDialog = new ProgressDialog(getContext());
    }
    private void initListerners () {
        recyclerview.addOnItemTouchListener(new MyRecyclerItemClickListener(getContext (), (view, position) -> {
            String feeditem = CATAGORIES_ARRAY[ position ];
            //Log.e("xxx", "initListerners:cata "+feeditem );
            startActivity(new Intent(
                            getContext (), CatagorySearchListing.class)
                            .putExtra("id_cata", feeditem)
                    /*.putExtra("regNumber", feeditem.getRegNumber ())*/
            );
        }));
    }

    private void setViewsValues () {
        mAdapter = new CatagoryRecyclerAdapter(getContext (), CATAGORIES_ARRAY);
        recyclerview.setAdapter(randomRecyclerViewAdapter(mAdapter));
        recyclerview.setHasFixedSize(true);
        recyclerview_status.setVisibility ( View.GONE );
     //   mAdapter.setitems( (ArrayList < AddsPojo >) CATAGORIES_ARRAY );

    }

    private void initViews () {

        recyclerview = layout.findViewById ( R.id.recyclerview );
        recyclerview_status = layout.findViewById ( R.id.recyclerview_status );
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext ());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutManager);
        recyclerview.addItemDecoration(new LinearItermDecorator(getContext (), LinearLayoutManager.VERTICAL));
        recyclerview.setItemAnimator(randomItemAnimator());

    }
    private RecyclerView.Adapter randomRecyclerViewAdapter(RecyclerView.Adapter adapter) {
        if (android.os.Build.VERSION.SDK_INT < 18) {
            // return new DefaultItemAnimator();
            return adapter;
        } else {

            int randomNum;
            int maximum = 4;
            int minimum = 0;
            randomNum = new Random().nextInt(maximum - minimum + 1) + minimum;

            switch (randomNum) {

                case 1:
                    AlphaInAnimationAdapter ada = new AlphaInAnimationAdapter(adapter);
                    ada.setDuration(2300);
                    return ada;
                case 2:
                    ScaleInAnimationAdapter adat = new ScaleInAnimationAdapter(adapter);
                    adat.setDuration(1000);
                    return adat;
                case 3:
                    SlideInBottomAnimationAdapter adap = new SlideInBottomAnimationAdapter(adapter);
                    adap.setDuration(1000);
                    return adap;
                case 4:
                    SlideInRightAnimationAdapter adax = new SlideInRightAnimationAdapter(adapter);
                    adax.setDuration(2100);
                    return adax;
                case 5:
                    SlideInLeftAnimationAdapter adas = new SlideInLeftAnimationAdapter(adapter);
                    adas.setDuration(2100);
                    return adas;
                default:
                    AlphaInAnimationAdapter adau = new AlphaInAnimationAdapter(adapter);
                    adau.setDuration(1000);
                    return adau;
            }
        }
    }
    private void showProgressLoading(boolean state, String doingWhat) {

        if (state) {
            progressDialog.setMessage(doingWhat + "...Please wait.");
            progressDialog.setCancelable(false);
            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }

        } else {
            progressDialog.dismiss();
        }

    }
    private RecyclerView.ItemAnimator randomItemAnimator() {
        if (android.os.Build.VERSION.SDK_INT < 18) {
            return new DefaultItemAnimator();

        } else {
            int randomNum;
            int maximum = 22;
            int minimum = 0;
            randomNum = new Random ().nextInt(maximum - minimum + 1) + minimum;

            switch (randomNum) {
                case 1:
                    return new ScaleInTopAnimator();
                case 2:
                    return new ScaleInAnimator();
                case 3:
                    return new ScaleInTopAnimator();
                case 4:
                    return new ScaleInBottomAnimator();
                case 5:
                    return new ScaleInLeftAnimator();
                case 6:
                    return new ScaleInRightAnimator();
                case 7:
                    return new FadeInAnimator();
                case 8:
                    return new FadeInDownAnimator();
                case 9:
                    return new FadeInUpAnimator();
                case 10:
                    return new FadeInLeftAnimator();
                case 11:
                    return new FadeInRightAnimator();
                case 12:
                    return new FlipInTopXAnimator();
                case 13:
                    return new FlipInBottomXAnimator();
                case 14:
                    return new FlipInLeftYAnimator();
                case 15:
                    return new FlipInRightYAnimator();
                case 16:
                    return new SlideInLeftAnimator();
                case 17:
                    return new SlideInRightAnimator();
                case 18:
                    return new OvershootInLeftAnimator();
                case 19:
                    return new OvershootInRightAnimator();
                case 20:
                    return new SlideInUpAnimator();
                case 21:
                    return new SlideInDownAnimator();
                case 22:
                    return new LandingAnimator();

                default:
                    return new ScaleInTopAnimator();
            }
        }
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_search) {


            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
