package com.mol.fragments.loogeduser;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.net.Uri;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.mol.DBAccess.Preffs;
import com.mol.R;
import com.mol.cwidgets.customfonts.MyEditText;
import com.mol.cwidgets.customfonts.MyTextView;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.mol.netwox.NetGet.saveAddPost;
import static com.mol.netwox.NetGet.savePostData;
import static com.mol.utils.utils.CATAGORIES_ARRAY;

/**
 * Created by Kajiva Kinsley on 10-Jun-18.
 */

public class AddPost_fragment extends Fragment {

    private View layout , parent;
    private Button btnSubmit ,selectMedia;
    private MyEditText  businesName,title,location,story;
    private Spinner catagory;private String  catagor_="";
    // Log tag that is used to distinguish log info.
    private final static String TAG_BROWSE_PICTURE = "BROWSE_PICTURE";

    // Used when request action Intent.ACTION_GET_CONTENT
    private final static int REQUEST_CODE_BROWSE_PICTURE = 1;

    // Used when request read external storage permission.
    private final static int REQUEST_PERMISSION_READ_EXTERNAL = 2;
    // Save user selected image uri list.
    private List<Uri> userSelectedImageUriList = new ArrayList<>();
    String mediaPath = "";
    private Preffs preffs ;

    // Currently displayed user selected image index in userSelectedImageUriList.
    private int currentDisplayedUserSelectImageIndex = 0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate( R.layout.addpost_fragment, container, false);
        preffs = new Preffs ( getContext () );
        initViews();
        setViewsValues();
        initListerners();
        return layout;
    }

    private void initListerners () {
        selectMedia.setOnClickListener ( ev->{
            // Because camera app returned uri value is something like file:///storage/41B7-12F1/DCIM/Camera/IMG_20180211_095139.jpg
            // So if show the camera image in image view, this app require below permission.
            int readExternalStoragePermission = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
            if(readExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            {
                String requirePermission[] = {Manifest.permission.READ_EXTERNAL_STORAGE};
                ActivityCompat.requestPermissions(getActivity (), requirePermission, REQUEST_PERMISSION_READ_EXTERNAL);
            }else {
                openPictureGallery();
            }
        } );
        btnSubmit.setOnClickListener ( ed->{
            String businesName_ = businesName.getText ().toString ().trim ();
            String title_ = title.getText ().toString ().trim ();
            String location_ = location.getText ().toString ().trim ();
            String story_ = story.getText ().toString ().trim ();
            String userID = preffs.getUSER_NAME ()+"";
            //Log.e ( "xxx", "1999initListerners: "+mediaPath.isEmpty () + "--"+mediaPath );

            /*if(mediaPath.isEmpty ()){
                Toast.makeText ( getContext(), "Mdeai Required !", Toast.LENGTH_SHORT ).show ();
                return;
            }*/
            if(businesName_.isEmpty ()){
                Toast.makeText ( getContext(), "Business Name Required !", Toast.LENGTH_SHORT ).show ();
                return;
            }
            if(title_.isEmpty ()){
                Toast.makeText ( getContext(), "Ttitle Required !", Toast.LENGTH_SHORT ).show ();
                return;
            }
            if(location_.isEmpty ()){
                Toast.makeText ( getContext(), "location Required !", Toast.LENGTH_SHORT ).show ();
                return;
            }
            if(story_.isEmpty ()){
                Toast.makeText ( getContext(), "Story Required !", Toast.LENGTH_SHORT ).show ();
                return;
            }

            new AsyncTask<Void,Void,String> (){
                ProgressDialog progressDialog;


                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressDialog=new ProgressDialog(getContext ());
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage("Uploading Please wait..");
                    progressDialog.show();

                }

                @Override
                protected void onPostExecute (String s) {
                    super.onPostExecute ( s );
                    if (progressDialog != null){
                        progressDialog.dismiss();
                    }
                    if(s.equals ( "done" )){
                        businesName.setText ( "" );
                        title.setText ( "" );
                        location.setText ( "" );
                        story.setText ( "" );
                        mediaPath="";
                        Toast.makeText(getContext(), "Saved", Toast.LENGTH_SHORT).show();
                    }else if(s.isEmpty()){
                        Toast.makeText(getContext(), "Failed to connect", Toast.LENGTH_LONG).show();
                    }
                    Log.e ( "xxx", "onPostExecute: "+s );
                }

                @Override
                protected String doInBackground (Void... voids) {
                    return savePostData(mResults , businesName_ ,title_,location_ ,story_ ,catagor_ ,userID);
                }

            }.execute (  );
        } );

    }
    private static final int REQUEST_CODE = 123;
    private ArrayList<String> mResults = new ArrayList<>();
    /* Invoke android os system file browser to select images. */
    private void openPictureGallery()
    {
        //showImagePopup();

        // start multiple photos selector
        Intent intent = new Intent(getActivity(), ImagesSelectorActivity.class);
// max number of images to be selected
        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 5);
// min size of image which will be shown; to filter tiny images (mainly icons)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100000);
// show camera or not
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
// pass current selected images as the initial value
        intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
// start the selector
        startActivityForResult(intent, REQUEST_CODE);
    }
    /* When the action Intent.ACTION_GET_CONTENT invoked app return, this method will be executed. */
    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_CODE) {
            if(resultCode == RESULT_OK) {
                mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                assert mResults != null;

                // show results in textview
                StringBuffer sb = new StringBuffer();
                sb.append(String.format("Totally %d images selected:", mResults.size())).append("\n");
                for(String result : mResults) {
                    sb.append(result).append("\n");
                }
                Log.e("xxxx", "onActivityResult: " + sb.toString() );
                //tvResults.setText(sb.toString());
            }
        }
        if (/*resultCode == RESULT_OK && requestCode == 1010*/ 2==3) {
                if (data == null) {

                    Snackbar.make(parent, "Faild to get the file", Snackbar.LENGTH_INDEFINITE).show();
                    return;
                }
                Uri selectedImageUri = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContext ().getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    mediaPath = cursor.getString(columnIndex);


                    cursor.close();


                } else {
                    Snackbar.make(parent, "unable to load file", Snackbar.LENGTH_LONG).setAction("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showImagePopup();
                        }
                    }).show();

            }
        }
    }
    private void showImagePopup() {
        final Intent galleryIntent = new Intent ();
        galleryIntent.setType("image/* video/*");
        galleryIntent.setAction(Intent.ACTION_PICK);
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Choose a Video");
        startActivityForResult(chooserIntent, 1010);
    }
    /* After user choose grant read external storage permission or not. */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==REQUEST_PERMISSION_READ_EXTERNAL)
        {
            if(grantResults.length > 0)
            {
                int grantResult = grantResults[0];
                if(grantResult == PackageManager.PERMISSION_GRANTED)
                {
                    // If user grant the permission then open choose image popup dialog.
                    openPictureGallery();
                }else
                {
                    Toast.makeText(getContext (), "You denied read external storage permission.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
    private void setViewsValues () {

        ArrayAdapter<String> adapter = new ArrayAdapter<String> (getContext (),
                android.R.layout.simple_spinner_item, CATAGORIES_ARRAY);
        adapter.setDropDownViewResource(android.R.layout.simple_spin‌​ner_dropdown_item);
        catagory.setAdapter(adapter);
        catagory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener ()
        {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                catagor_=CATAGORIES_ARRAY[position];
              //  Toast.makeText(getContext (), arraySpinner[position], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    private void initViews () {
        parent = layout.findViewById ( R.id.parent );
        btnSubmit = layout.findViewById ( R.id.btnSubmit );
        selectMedia = layout.findViewById ( R.id.selectMedia );
        businesName = layout.findViewById ( R.id.businesName );
        title = layout.findViewById ( R.id.title );
        location = layout.findViewById ( R.id.location );
        story = layout.findViewById ( R.id.story );
        catagory = layout.findViewById ( R.id.catagory );

    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_search) {


            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
