package com.mol.fragments.loogeduser;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.SlideInBottomAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.SlideInRightAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;
import jp.wasabeef.recyclerview.animators.FadeInDownAnimator;
import jp.wasabeef.recyclerview.animators.FadeInLeftAnimator;
import jp.wasabeef.recyclerview.animators.FadeInRightAnimator;
import jp.wasabeef.recyclerview.animators.FadeInUpAnimator;
import jp.wasabeef.recyclerview.animators.FlipInBottomXAnimator;
import jp.wasabeef.recyclerview.animators.FlipInLeftYAnimator;
import jp.wasabeef.recyclerview.animators.FlipInRightYAnimator;
import jp.wasabeef.recyclerview.animators.FlipInTopXAnimator;
import jp.wasabeef.recyclerview.animators.LandingAnimator;
import jp.wasabeef.recyclerview.animators.OvershootInLeftAnimator;
import jp.wasabeef.recyclerview.animators.OvershootInRightAnimator;
import jp.wasabeef.recyclerview.animators.ScaleInAnimator;
import jp.wasabeef.recyclerview.animators.ScaleInBottomAnimator;
import jp.wasabeef.recyclerview.animators.ScaleInLeftAnimator;
import jp.wasabeef.recyclerview.animators.ScaleInRightAnimator;
import jp.wasabeef.recyclerview.animators.ScaleInTopAnimator;
import jp.wasabeef.recyclerview.animators.SlideInDownAnimator;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.SlideInRightAnimator;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import com.mol.Adapters.MylistingRecyclerAdapter;
import com.mol.Pojos.AddsPojo;
import com.mol.R;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.mol.activities.CatagorySearchListing;
import com.mol.activities.ListingReadPage;
import com.mol.cwidgets.LinearItermDecorator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import com.mol.cwidgets.MyRecyclerItemClickListener;
import android.animation.Animator;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static com.mol.netwox.NetGet.getListings;
import static com.mol.utils.utils.startListReadPage;

/**
 * Created by Kajiva Kinsley on 10-Jun-18.
 */

public class MylistingFragment  extends Fragment   {
    private View layout;
    private TextView recyclerview_status;
    private RecyclerView recyclerview;
    private ProgressDialog progressDialog;
    private final static int VERTICAL_ITEM_SPACE = 4;//14
    private MylistingRecyclerAdapter mAdapter;
    private ArrayList<AddsPojo > myOptions = new ArrayList<> ();
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private ViewPager viewPager;
    private View searchAppBarLayout;
    private AppBarLayout appBar;
    private float positionFromRight = 2;
    private Toolbar toolbar,searchToolBar;
    private EditText searchEditText;
    private LinearLayout searchLayput;
    private ImageButton clear_txt;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate( R.layout.mylistings_fragment, container, false);
        initObjects();
        initViews();
        setViewsValues();
        initListerners();

        return layout;
    }

    private void initObjects() {
        progressDialog = new ProgressDialog(getContext());
    }

    private void initListerners () {
        recyclerview.addOnItemTouchListener(new MyRecyclerItemClickListener (getContext (), (view, position) -> {
            AddsPojo feeditem = myOptions.get ( position );

            startListReadPage(getContext(), feeditem);
        }));
    }

    private void setViewsValues () {
        mAdapter = new MylistingRecyclerAdapter(getContext (), myOptions);
        recyclerview.setAdapter(randomRecyclerViewAdapter(mAdapter));
        recyclerview.setHasFixedSize(true);
        //showProgressLoading(true , "Loading...");
        new AsyncTask<Void,Void,List<AddsPojo>> (){

            @Override
            protected List < AddsPojo > doInBackground (Void... voids) {
                return getListings("");
            }

            @Override
            protected void onPostExecute (List < AddsPojo > addsPojos) {
                super.onPostExecute ( addsPojos );
                //showProgressLoading(false,"");
                mySwipeRefreshLayout.setRefreshing(false);
                if(addsPojos.isEmpty ()){
                    recyclerview_status.setVisibility ( View.VISIBLE );
                }else{
                    recyclerview_status.setVisibility ( View.GONE );
                    myOptions .addAll( addsPojos);
                    mAdapter.setitems( myOptions );
                }
            }
        }.execute (  );

    }

    private void initViews () {
        recyclerview = layout.findViewById ( R.id.recyclerview );
        recyclerview_status = layout.findViewById ( R.id.recyclerview_status );
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext ());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutManager);
        recyclerview.addItemDecoration(new LinearItermDecorator (getContext (), LinearLayoutManager.VERTICAL));
        recyclerview.setItemAnimator(randomItemAnimator());
        mySwipeRefreshLayout = layout.findViewById(R.id.swiperefresh);
        mySwipeRefreshLayout.setRefreshing(true);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
        searchLayput= (LinearLayout) getActivity().findViewById(R.id.searchLayput);
        appBar = (AppBarLayout) getActivity().findViewById(R.id.appBar);

        searchAppBarLayout = getActivity().findViewById(R.id.layout_appbar_search);
        searchToolBar = (Toolbar) getActivity().findViewById(R.id.toolbar_search);
        searchEditText = (EditText) getActivity().findViewById(R.id.editText_search);
        clear_txt= (ImageButton) getActivity().findViewById(R.id.clear_txt);
        clear_txt.setOnClickListener(view -> {
                    if (searchEditText.getText().toString().trim().isEmpty()) {
                        if (searchAppBarLayout.getVisibility() == View.VISIBLE)
                            hideSearchBar(positionFromRight);
                    } else {
                        searchEditText.setText("");
                    }

                }
        );


        // if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        layout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            // @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                v.removeOnLayoutChangeListener(this);
                initSearchBar();
            }
        });
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
    }
    private void initSearchBar() {

        if (searchToolBar != null) {
            searchToolBar.setNavigationIcon( R.drawable.ic_arrow_back_white_24dp );
            searchAppBarLayout.setVisibility(View.GONE);
            searchToolBar.setNavigationOnClickListener(view -> hideSearchBar(positionFromRight));
        }
    }
    /**
     * to show the searchAppBarLayout and hide the mainAppBar with animation.
     *
     * @param positionFromRight
     */

    private void showSearchBar(float positionFromRight) {
        AnimatorSet set = new AnimatorSet();

        set.playTogether(
               // ObjectAnimator.ofFloat(appBar, "translationY", -tabs.getHeight()),
              //  ObjectAnimator.ofFloat(viewPager, "translationY", -tabs.getHeight()),
                ObjectAnimator.ofFloat(appBar, "alpha", 0)
        );
        set.setDuration(300).addListener(new com.nineoldandroids.animation.Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(com.nineoldandroids.animation.Animator animation) {

            }

            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                appBar.setVisibility(View.GONE);
                searchEditText.requestFocus();

               showKeyBoard(searchEditText);

                searchEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        final String query = charSequence.toString();
                        final ArrayList<AddsPojo> filteredModelList = (ArrayList<AddsPojo>) filter(myOptions, query);

                        if (filteredModelList != null) {
                            mAdapter.update(filteredModelList);

                            recyclerview.scrollToPosition(0);


                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
            }

            @Override
            public void onAnimationCancel(com.nineoldandroids.animation.Animator animation) {

            }

            @Override
            public void onAnimationRepeat(com.nineoldandroids.animation.Animator animation) {

            }
        });
        set.start();


        // start x-index for circular animation
        int cx = (int) (toolbar.getWidth() -  48 * (0.5f + positionFromRight));
        // start y-index for circular animation
        int cy = (toolbar.getTop() + toolbar.getBottom()) / 2;

        // calculate max radius
        int dx = Math.max(cx, toolbar.getWidth() - cx);
        int dy = Math.max(cy, toolbar.getHeight() - cy);
        float finalRadius = (float) Math.hypot(dx, dy);

        // Circular animation declaration begin
        final Animator animator;
        animator = io.codetail.animation.ViewAnimationUtils
                .createCircularReveal(searchAppBarLayout, cx, cy, 0, finalRadius);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setDuration(200);
        searchAppBarLayout.setVisibility(View.VISIBLE);

        animator.start();
        // Circular animation declaration end
    }
    /**
     * to hide the searchAppBarLayout and show the mainAppBar with animation.
     *
     * @param positionFromRight
     */
    private void hideSearchBar(float positionFromRight) {

        // start x-index for circular animation
        int cx = (int) (toolbar.getWidth() - 48 * (0.5f + positionFromRight));
        // start  y-index for circular animation
        int cy = (toolbar.getTop() + toolbar.getBottom()) / 2;

        // calculate max radius
        int dx = Math.max(cx, toolbar.getWidth() - cx);
        int dy = Math.max(cy, toolbar.getHeight() - cy);
        float finalRadius = (float) Math.hypot(dx, dy);

        /**/



        /**/

        // Circular animation declaration begin
        Animator animator;
        animator = io.codetail.animation.ViewAnimationUtils
                .createCircularReveal(searchAppBarLayout, cx, cy, finalRadius, 0);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setDuration(200);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                searchAppBarLayout.setVisibility(View.GONE);
               hideKeyBoard(searchEditText);

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        animator.start();
        // Circular animation declaration end

        appBar.setVisibility(View.VISIBLE);
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(appBar, "translationY", 0),
                ObjectAnimator.ofFloat(appBar, "alpha", 1)
                ,ObjectAnimator.ofFloat(viewPager, "translationY", 0)
        );
        set.setDuration(100).start();

    }
    void hideKeyBoard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    void showKeyBoard(View view) {
        if (view != null) {
            if (view instanceof EditText) {
                EditText editText = (EditText) view;
                editText.setSelection(editText.length());
            }
            InputMethodManager imm = (InputMethodManager)
                    view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }
    private RecyclerView.Adapter randomRecyclerViewAdapter(RecyclerView.Adapter adapter) {
        if (android.os.Build.VERSION.SDK_INT < 18) {
            // return new DefaultItemAnimator();
            return adapter;
        } else {

            int randomNum;
            int maximum = 4;
            int minimum = 0;
            randomNum = new Random().nextInt(maximum - minimum + 1) + minimum;

            switch (randomNum) {

                case 1:
                    AlphaInAnimationAdapter ada = new AlphaInAnimationAdapter(adapter);
                    ada.setDuration(2300);
                    return ada;
                case 2:
                    ScaleInAnimationAdapter adat = new ScaleInAnimationAdapter(adapter);
                    adat.setDuration(1000);
                    return adat;
                case 3:
                    SlideInBottomAnimationAdapter adap = new SlideInBottomAnimationAdapter(adapter);
                    adap.setDuration(1000);
                    return adap;
                case 4:
                    SlideInRightAnimationAdapter adax = new SlideInRightAnimationAdapter(adapter);
                    adax.setDuration(2100);
                    return adax;
                case 5:
                    SlideInLeftAnimationAdapter adas = new SlideInLeftAnimationAdapter(adapter);
                    adas.setDuration(2100);
                    return adas;
                default:
                    AlphaInAnimationAdapter adau = new AlphaInAnimationAdapter(adapter);
                    adau.setDuration(1000);
                    return adau;
            }
        }
    }
    private void showProgressLoading(boolean state, String doingWhat) {

        if (state) {
            progressDialog.setMessage(doingWhat + "...Please wait.");
            progressDialog.setCancelable(false);
            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }

        } else {
            progressDialog.dismiss();
        }

    }
    private RecyclerView.ItemAnimator randomItemAnimator() {
        if (android.os.Build.VERSION.SDK_INT < 18) {
            return new DefaultItemAnimator ();

        } else {
            int randomNum;
            int maximum = 22;
            int minimum = 0;
            randomNum = new Random ().nextInt(maximum - minimum + 1) + minimum;

            switch (randomNum) {
                case 1:
                    return new ScaleInTopAnimator();
                case 2:
                    return new ScaleInAnimator();
                case 3:
                    return new ScaleInTopAnimator();
                case 4:
                    return new ScaleInBottomAnimator();
                case 5:
                    return new ScaleInLeftAnimator();
                case 6:
                    return new ScaleInRightAnimator();
                case 7:
                    return new FadeInAnimator();
                case 8:
                    return new FadeInDownAnimator();
                case 9:
                    return new FadeInUpAnimator();
                case 10:
                    return new FadeInLeftAnimator();
                case 11:
                    return new FadeInRightAnimator();
                case 12:
                    return new FlipInTopXAnimator();
                case 13:
                    return new FlipInBottomXAnimator();
                case 14:
                    return new FlipInLeftYAnimator();
                case 15:
                    return new FlipInRightYAnimator();
                case 16:
                    return new SlideInLeftAnimator();
                case 17:
                    return new SlideInRightAnimator();
                case 18:
                    return new OvershootInLeftAnimator();
                case 19:
                    return new OvershootInRightAnimator();
                case 20:
                    return new SlideInUpAnimator();
                case 21:
                    return new SlideInDownAnimator();
                case 22:
                    return new LandingAnimator();

                default:
                    return new ScaleInTopAnimator();
            }
        }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }


    private List<AddsPojo> filter(List<AddsPojo> models, String query) {
        query = query.toLowerCase().trim();
        final List<AddsPojo> filteredModelList = new ArrayList<>();
        for (AddsPojo model : models) {
            final String text = model.getLocation ().toLowerCase();
            final String businesName = model.getBusinesName ();
            final String title = model.getTitle ();
            if (text.contains(query) || text.contains(businesName) || text.contains(title) ) {

                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_search) {

            showSearchBar(positionFromRight);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
