package com.mol.fragments.loogeduser;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.mol.R;

/**
 * Created by Kajiva Kinsley on 10-Jun-18.
 */

public class PricingFragment  extends Fragment {
    private View layout;
    private final static int VERTICAL_ITEM_SPACE = 4;//14

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate( R.layout.newitems_fragment, container, false);
        initViews();
        setViewsValues();
        initListerners();

        return layout;
    }

    private void initListerners () {

    }

    private void setViewsValues () {

    }

    private void initViews () {


    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_search) {


            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
